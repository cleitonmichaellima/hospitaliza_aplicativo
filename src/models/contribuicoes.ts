 import {AvaliacaoInstituicao} from "./avaliacao-instituicao";
 import {Usuario} from "./usuario";
 import {Instituicao} from "./instituicao";

export class Contribuicoes {
	id_contribuicao : number;
	id_usuario: Usuario;
	id_instituicao: Instituicao;
	textoContribuicao: string;
	avaliacao:AvaliacaoInstituicao;

}