import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { InstituicaoPesquisa } from '../../models/instituicaopesquisa';

@Injectable()
export class Instituicao {
  items: InstituicaoPesquisa[] = [];

  defaultItem: any = {
    "name": "Burt Bear",
    "profilePic": "assets/img/speakers/bear.jpg",
    "about": "Burt is a Bear.",
  };


  constructor(public http: Http) {
    let items = [
      {
        "name": "Hospital Santa Casa",
        "profilePic": "assets/img/speakers/bear.jpg",
        "about": "Av Indepência 1405."
      }

    ];

    for (let item of items) {
      this.items.push(new InstituicaoPesquisa(item));
    }
  }

  query(params?: any) {
    if (!params) {
      return this.items;
    }

    return this.items.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  add(item: InstituicaoPesquisa) {
    this.items.push(item);
  }

  delete(item: InstituicaoPesquisa) {
    this.items.splice(this.items.indexOf(item), 1);
  }
}
