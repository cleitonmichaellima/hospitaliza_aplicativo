import { Component } from '@angular/core';
import { NavController, NavParams,ModalController } from 'ionic-angular';
import { ItemCreatePage } from '../item-create/item-create';
import { Instituicao } from '../../providers/providers';
import { Items } from '../../providers/providers';
import { Item } from '../../models/item';
import { ItemDetailPage } from '../item-detail/item-detail';

@Component({
  selector: 'page-instituicao-detail-detail',
  templateUrl: 'instituicao-detail.html'
})
export class InstituicaoDetailPage {
  item: any;
  currentItems: any = [];
  constructor(public navCtrl: NavController, navParams: NavParams, public Instituicao: Instituicao, public items: Items,public modalCtrl: ModalController) {
    this.item = navParams.get('item') || Instituicao.defaultItem;
    this.currentItems = this.items.query();

  }

  addItem() {
    let addModal = this.modalCtrl.create(ItemCreatePage);
    addModal.onDidDismiss(item => {
      if (item) {
        this.items.add(item);
      }
    })
    addModal.present();
  }

  openItem(item: Item) {
    this.navCtrl.push(ItemDetailPage, {
      item: item
    });
  }



}
