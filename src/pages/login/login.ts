import { Component } from '@angular/core';
import { NavController, ToastController,  MenuController } from 'ionic-angular';
//import { MainPage } from '../../pages/pages';
import {LoginProvider} from "../../providers/login/login";
import {UsuarioProvider} from "../../providers/usuario";
import { User } from '../../providers/user';
import { PerfilPage } from "../perfil/perfil";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { email: string, password: string } = {
    email: 'test@example.com',
    password: 'test'
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    public menuCtrl: MenuController,
    public loginProvider: LoginProvider,
    public UsuarioProvider: UsuarioProvider) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  }

  // Attempt to login in through our User service
  ionViewDidEnter(){
      this.menuCtrl.enable(false);
      this.menuCtrl.swipeEnable(false);
    }

    ionViewDidLoad() {
      this.loginProvider.loginSucessoEventEmitter.subscribe(
        user => {
          console.log(user.providerData[0]);

          this.UsuarioProvider.nome=user.providerData[0].displayName;
          this.UsuarioProvider.foto=user.providerData[0].photoURL;
          this.menuCtrl.enable(true);
          this.menuCtrl.swipeEnable(true);
          this.navCtrl.setRoot(PerfilPage);

        }
      );
      this.loginProvider.loginFalhaEventEmitter.subscribe(
        error => console.log(error)
      )
    }

    loginComFacebook(){
      this.loginProvider.loginComFacebook();
    }

}
