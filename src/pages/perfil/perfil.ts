import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ItemCreatePage } from '../item-create/item-create';
import { ItemDetailPage } from '../item-detail/item-detail';
import { InstituicaoDetailPage } from '../instituicao-detail/instituicao-detail';

import { Item } from '../../models/item';

import { Items } from '../../providers/providers';
import { Instituicao } from '../../providers/providers';
import { UsuarioProvider } from '../../providers/usuario';


/**
 * Generated class for the PerfilPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})


export class PerfilPage {
  currentItems: any = [];
  cardItems: any[];
  currentInstituicao: any = [];
  UsuarioLogado: any = [];
  fotoUsuario:any = [];


  constructor(public navCtrl: NavController, public navParams: NavParams,public items: Items, public Instituicao: Instituicao, public modalCtrl: ModalController, public Usuario: UsuarioProvider) {
  this.currentItems = this.items.query();
    this.cardItems = [
      {
        user: {
          avatar: 'assets/img/marty-avatar.png',
          name: 'Marty McFly'
        },
        date: 'November 5, 1955',
        image: 'assets/img/advance-card-bttf.png',
        content: 'Wait a minute. Wait a minute, Doc. Uhhh... Are you telling me that you built a time machine... out of a DeLorean?! Whoa. This is heavy.',
        note: 'Wait a minute. Wait a minute, Doc. Uhhh... Are you telling me that you built a time machine... out of a DeLorean?! Whoa. This is heavy.',
      }
    ]

    this.UsuarioLogado =this.Usuario.nome;
    this.fotoUsuario =this.Usuario.foto;





  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
  }/*
   * Perform a service for the proper items.
   */


   addItem() {
     let addModal = this.modalCtrl.create(ItemCreatePage);
     addModal.onDidDismiss(item => {
       if (item) {
         this.items.add(item);
       }
     })
     addModal.present();
   }

   /**
    * Delete an item from the list of items.
    */
   deleteItem(item) {
     this.items.delete(item);
   }


   /**
    * Navigate to the detail page for this item.
    */
   openItem(item: Item) {
     this.navCtrl.push(ItemDetailPage, {
       item: item
     });
   }

   getInstituicao(ev) {
     let val = ev.target.value;
     if (!val || !val.trim()) {
       this.currentInstituicao = [];
       return;
     }
     this.currentInstituicao = this.Instituicao.query({
       name: val
     });
    }

    openInstituicao(item: Item) {
     this.navCtrl.push(InstituicaoDetailPage, {
       item: item
     });
    }

}

/// pesquisa de instituicoes
/*
getItems(ev) {
 let val = ev.target.value;
 if (!val || !val.trim()) {
   this.currentItems = [];
   return;
 }
 this.currentItems = this.items.query({
   name: val
 });
}

/**
* Navigate to the detail page for this item.

openItem(item: Item) {
 this.navCtrl.push(ItemDetailPage, {
   item: item
 });
}*/

export class ListMasterPage {


  constructor(public navCtrl: NavController, public items: Items, public modalCtrl: ModalController) {


  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
  }

  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addItem() {
    let addModal = this.modalCtrl.create(ItemCreatePage);
    addModal.onDidDismiss(item => {
      if (item) {
        this.items.add(item);
      }
    })
    addModal.present();
  }

  /**
   * Delete an item from the list of items.
   */
  deleteItem(item) {
    this.items.delete(item);
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push(ItemDetailPage, {
      item: item
    });
  }
}

export class CardsPage {
  cardItems: any[];

  constructor(public navCtrl: NavController) {
    this.cardItems = [
      {
        user: {
          avatar: 'assets/img/marty-avatar.png',
          name: 'Marty McFly'
        },
        date: 'November 5, 1955',
        image: 'assets/img/advance-card-bttf.png',
        content: 'Wait a minute. Wait a minute, Doc. Uhhh... Are you telling me that you built a time machine... out of a DeLorean?! Whoa. This is heavy.',
      },
      {
        user: {
          avatar: 'assets/img/sarah-avatar.png.jpeg',
          name: 'Sarah Connor'
        },
        date: 'May 12, 1984',
        image: 'assets/img/advance-card-tmntr.jpg',
        content: 'I face the unknown future, with a sense of hope. Because if a machine, a Terminator, can learn the value of human life, maybe we can too.'
      },
      {
        user: {
          avatar: 'assets/img/ian-avatar.png',
          name: 'Dr. Ian Malcolm'
        },
        date: 'June 28, 1990',
        image: 'assets/img/advance-card-jp.jpg',
        content: 'Your scientists were so preoccupied with whether or not they could, that they didn\'t stop to think if they should.'
      }
    ];

  }
  }
  export class SearchPage {

    currentItems: any = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, public items: Items) { }

    /**
     * Perform a service for the proper items.
     */
    getItems(ev) {
      let val = ev.target.value;
      if (!val || !val.trim()) {
        this.currentItems = [];
        return;
      }
      this.currentItems = this.items.query({
        name: val
      });
    }

    /**
     * Navigate to the detail page for this item.
     */
    openItem(item: Item) {
      this.navCtrl.push(ItemDetailPage, {
        item: item
      });
    }

  }
