import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NovaContribuicaoPage } from './nova-contribuicao';

@NgModule({
  declarations: [
    NovaContribuicaoPage,
  ],
  imports: [
    IonicPageModule.forChild(NovaContribuicaoPage),
  ],
  exports: [
    NovaContribuicaoPage
  ]
})
export class NovaContribuicaoPageModule {}
